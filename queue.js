let collection = [];

// Write the queue functions below.

function print () {
    // console.log(collection);
    return collection
}

// print();

function enqueue(item) {
    collection = [...collection, item]
    return collection
}

// enqueue("John");
// print();
// enqueue("Jane");
// print();

function dequeue() {
    let item;
    [item, ...collection] = collection;
    return collection
}

// dequeue();
// print();
// enqueue("Bob");
// print();
// enqueue("Cherry");
// print();

function front () {
    if(isEmpty() === false) {
        // console.log(collection[0]);
        return collection[0]
    }
}

// front();

function size () {
    // console.log(collection.length);
    return collection.length
}

// size();

function isEmpty() {
    return collection.length === 0
}

// isEmpty();

module.exports = {

collection,
print,
enqueue,
dequeue,
front,
size,
isEmpty

};